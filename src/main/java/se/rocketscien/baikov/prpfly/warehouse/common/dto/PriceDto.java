package se.rocketscien.baikov.prpfly.warehouse.common.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;

@Value
@Builder
@Jacksonized
@Schema
public class PriceDto {

    @Schema(title = "Product ID")
    Long productId;

    @Schema(title = "Price")
    BigDecimal price;

}
