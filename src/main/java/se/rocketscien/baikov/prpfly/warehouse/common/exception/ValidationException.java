package se.rocketscien.baikov.prpfly.warehouse.common.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ValidationException extends RuntimeException {

    private final HttpStatus httpStatus;
    private final Object[] args;

    public ValidationException(HttpStatus httpStatus, String code, Object... args) {
        super(code);
        this.httpStatus = httpStatus;
        this.args = args;
    }

}
