package se.rocketscien.baikov.prpfly.warehouse.common;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import se.rocketscien.baikov.prpfly.warehouse.common.exception.WarehouseCommonExceptionHandler;

@Configuration
public class WarehouseCommonConfiguration {

    @Bean
    public WarehouseCommonExceptionHandler warehouseCommonExceptionHandler(MessageSource messageSource) {
        return new WarehouseCommonExceptionHandler(messageSource);
    }

}
