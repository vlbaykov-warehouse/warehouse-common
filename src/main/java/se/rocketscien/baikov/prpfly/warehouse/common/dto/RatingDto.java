package se.rocketscien.baikov.prpfly.warehouse.common.dto;

import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

import java.math.BigDecimal;

@Value
@Builder
@Jacksonized
public class RatingDto {

    Long productId;

    BigDecimal rating;

}
