package se.rocketscien.baikov.prpfly.warehouse.common.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Schema(
        title = "Response Model",
        description = "If request succeeded then field 'result' will contain response, if any. " +
                "If error occurred while request execution then field 'error' will contain details about."
)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@EqualsAndHashCode
public class ResponseDto<T> {

    @Schema(title = "Request execution result")
    private T result;

    @Schema(title = "Request execution error details")
    private ResponseErrorDto error;

    public static <T> ResponseDto<T> result(T result) {
        return new ResponseDto<>(result, null);
    }

    public static <T> ResponseDto<T> empty() {
        return new ResponseDto<>();
    }

    public static <T> ResponseDto<T> error(ResponseErrorDto error) {
        return new ResponseDto<>(null, error);
    }

    @AllArgsConstructor
    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    @Getter
    @EqualsAndHashCode
    @ToString
    public static class ResponseErrorDto {
        private int code;
        private String message;
    }

}
